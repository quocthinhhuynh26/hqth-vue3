#base image
FROM node:19

#set working directory
WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY package.json package-lock.json index.html vite.config.js ./

RUN npm install
