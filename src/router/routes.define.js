import HomeView from "../views/HomeView.vue";
import LandingPage from "../views/LandingPage.vue";
const exports = [
  {
    path: "/",

    component: LandingPage,
    children: [
      {
        path: "",
        component: HomeView,
        name: "Home Landing Page",
      },
      {
        path: "home",
        component: HomeView,
        name: "Home Landing Page 2",
      },
      {
        path: "about",
        component: () => import("../views/AboutView.vue"),
        name: "About Landing Page",
      },
    ],
  },

  {
    path: "/404",
    name: "Page Not Found",
    component: () => import("../views/PageNotFound.vue"),
  },
  {
    path: "/:catchAll(.*)",
    redirect: "/404",
  },
];

export default exports;
